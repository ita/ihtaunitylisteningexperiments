# IHTA Unity Listening Experiments 

IHTA Unity Listening Experiments is a Framework for conducting listening experiments in VR. To make it easier for researchers, we developed a framework to conduct listening experiments in VR with the help of Unity® Software and Virtual Acoustics (VA).
The Framework has different kind of element types, that can be split into two categories. 
The first is related to the questionnaire and the second to events or commands. The questionnaire category contains different types of questions, such as conditional questions, pair comparisons, or questions with the possibility to answer on a discrete, categorical or floating scale. All question types can be organised into sets, which can be balanced.
The second category, events, can change the virtual environment, by moving or manipulating GameObjects. The sound GameObjects are connected via VAUnity to VA, delivering real-time updates. Likewise, auralization parameters and settings can be manipulated through GameObjects.


## Installation

Add the package to the maifest file in 'Packages/manifest.json' as shown below.


```json
{
    "dependencies": {
        "com.ihta-aachen.ihta-unity-listening-experiments": "https://git.rwth-aachen.de/ahe/IHTAUnityListeningExperiments#upm"
    }
}
```

## Example Scenes

After installation the sample scenes can be opend.

#### SampleQuestinnaire

A very simple questionaire with one 2 diffrent questiontypes and no audio included.

#### SampleQuestinnairewithVA

A very simple questionaire, where a sound is played through VA and then a 2 diffrent types of questions are asked.


## Dependencies

This software is using Virtual Acoustics (VA) from the Institute for Hearing Technology and Acoustics, RWTH Aachen University. <http://virtualacoustics.org/VA>