﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public abstract class EventProcedure : MonoBehaviour,IEventProcedure
{
    protected UnityEvent nextEvent=>controller.nextEvent;
    [SerializeField][HideInInspector] protected EventProcedureController controller;
    public string Name => procedureName;
    public int Repetitions => repetitions;

    [FormerlySerializedAs("name")] [SerializeField] protected string procedureName;
    [SerializeField] protected int repetitions;


    public abstract void OnStart(EventProcedureController controller);
    public abstract void OnEnd();
}