﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


[Serializable]
public class EP_Feedback : EventProcedure
{
    [SerializeField] private GameObject feedbackCanvas;

    [SerializeField]
    private InputResult[] radio;

    [SerializeField] private Text errorText = null;
    
   
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        foreach (var buttonController in radio)
        {
            buttonController.Start();
        }
        feedbackCanvas.SetActive(true);
    }

    public void Submit()
    {
        for (var i = 0; i < radio.Length; i++)
        {
            var result = radio[i].Result;
            if (!radio[i].ResultGiven)
            {
                // Error
                if (errorText != null)
                {
                    errorText.text = "Thick a question!";
                    controller.Log($"Situ {SceneManager.GetActiveScene().name.Replace(" ", "_")} Question {Name}_{i + 1} Given {result} ");
                    return;
                }
            }
            controller.Log($"Situ {SceneManager.GetActiveScene().name.Replace(" ", "_")} Question {Name}_{i + 1} Given {result}");
        }
        
        nextEvent.Invoke();
        OnEnd();
    }

    public override void OnEnd()
    {
        if (errorText != null)
        {
            errorText.text = "";
        }

        feedbackCanvas.SetActive(false);
    }


}