﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{

    public Text NumText;

    private int _number = 0;

    public int Number
    {
        get
        {
            return _number;
        }
        set
        {
            _number = value;
        }
    }

    public bool TextEnabled
    {
        get
        {
            if (NumText == null)
                return true;
            return NumText.text != "";
        }
        set
        {
            if(NumText == null)
                Debug.LogError("Assign NumText.");
            NumText.text = value ? Number.ToString() : "";
        }
    }

    public bool ObjectEnabled
    {
        get { return gameObject.activeSelf; }
        set { gameObject.SetActive(value); }
    }
	// Use this for initialization
	void Start ()
	{
	}
    
}
