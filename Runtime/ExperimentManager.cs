using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SharpOSC;

[Serializable]
public enum ExperimentSetting
{
    Visual=0,
    Audio=1,
    AudioVisual=2
}

[CreateAssetMenu(fileName = "ExperimentManager", menuName = "ExperimentManager", order = 0)]
public class ExperimentManager : ScriptableObject
{
    public bool manual = false;

    public string[] Scenes;
    public int participantId;
    [SerializeField] [HideInInspector] private int participantIdShadow;
    [SerializeField] [HideInInspector] private int sceneIndex = 0;
    [SerializeField] [HideInInspector] private LatinSquare sceneSquare;
    [SerializeField] private string endScene;
    [SerializeField] private string pauseScene;
    [SerializeField] private string loadingScene;

    [HideInInspector]public float progress;
    [HideInInspector]public string sceneQue;
    public string path;

    private Action onLoaderCallback;
    private SharpOSC.UDPSender sender = null;
    public SharpOSC.UDPSender Sender => sender ??= new SharpOSC.UDPSender("127.0.0.1", 55555);

    public void OnValidate()
    {
        if (participantIdShadow != participantId)
        {
            participantIdShadow = participantId;
            ResetSceneIndex();
        }

        if (Scenes.Length > 0)
        {
            sceneSquare = new LatinSquare(Scenes.Length);
        }
    }

    public void ResetSceneIndex()
    {
        sceneIndex = -1;
        
        var timeString = DateTime.Now.ToString("yyyyMMdd-HHmmss");
        path = @"" + timeString + "-Participant" + participantId + ".txt";
    }

    public void OnEnable()
    {
        OnValidate();
    }

    public void LoadNextScene()
    {
        if (manual)
        {
            sceneIndex++;
            if (sceneIndex < Scenes.Length)
                LoadSceneWithLoadingBar( Scenes[sceneIndex]);
            else
            {
                sceneIndex = -1;
                LoadSceneWithLoadingBar( endScene);
            }
        }
        else
        {
            sceneIndex++;
            if (sceneIndex < Scenes.Length)
                LoadSceneWithLoadingBar(Scenes[sceneSquare.GetIndex(participantId, sceneIndex)]);
            else
            {
                sceneIndex = -1;
                LoadSceneWithLoadingBar( endScene);
            }
        }
    }

    private void LoadSceneWithLoadingBar(string scene)
    {
        onLoaderCallback = () =>
        {
            SceneManager.LoadScene(scene);
        };
        SceneManager.LoadScene(loadingScene);
        
    }
    
    public void LoadingCallback()
    {
        if (onLoaderCallback != null)
        {
            onLoaderCallback();
            onLoaderCallback = null;
        }
        
    }
}