using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class EP_StartWithEventCanvas : EventProcedure
{
   
    [SerializeField] private GameObject canvas;
    [SerializeField] private UnityEvent unityEvent;
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        canvas.SetActive(true);
    }

    public void PressNextButton()
    {
        nextEvent.Invoke();
        OnEnd();
    }

    public override void OnEnd()
    {
        canvas.SetActive(false);
        unityEvent.Invoke();
    }
}