﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class EP_FeedbackQuestionBlock : EventProcedure
{
    [SerializeField] private GameObject feedbackCanvas;

    [SerializeField]
    private InputResult[] radio;

    [SerializeField] private Text errorText = null;
    [SerializeField] private Text questionText;
    [SerializeField] private string[] questions;
    private int questionId;
    
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        foreach (var buttonController in radio)
        {
            buttonController.Start();
        }
        feedbackCanvas.SetActive(true);
        questionId = 0;
        questionText.text = questions[questionId];
    }

    private void NextQuestion()
    {
        questionId++;
        if (questionId < questions.Length)
        {
            Reset();

            questionText.text = questions[questionId];
        }
        else
        {
            nextEvent.Invoke();
            OnEnd();
        }
    }

    private void Reset()
    {
        foreach (var radioButton in radio)
        {
            radioButton.Reset();
        }

        if (errorText != null)
        {
            errorText.text = "";
        }
    }

    public void Submit()
    {
        for (var i = 0; i < radio.Length; i++)
        {
            var result = radio[i].Result;
            if (!radio[i].ResultGiven)
            {
                // Error
                if (errorText != null)
                {
                    errorText.text = "Thick a question!";
                    controller.Log($"Situ { SceneManager.GetActiveScene().name.Replace(" ", "_")} Question {Name}_{questionId}_{i + 1} Given {result} ");
                    return;
                }
            }
            controller.Log($"Situ {SceneManager.GetActiveScene().name.Replace(" ", "_")} Question {Name}_{questionId}_{i + 1} Given {result}");
        }
        NextQuestion();
    }

    public override void OnEnd()
    {
        Reset();

        feedbackCanvas.SetActive(false);
    }
}