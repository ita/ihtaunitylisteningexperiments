using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EP_GameobjectsSetActive : EventProcedure
{
    public GameObject[] gameObjects;
    public bool state;
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;

        foreach (var gameObject in gameObjects)
        {
            gameObject.SetActive(state);
        }
        
        controller.nextEvent.Invoke();
    }

    public override void OnEnd()
    {
    }
}