using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SliderUpdater : MonoBehaviour
{
    public Slider slider;
    public ExperimentManager manager;


    private void Start()
    {
        slider.value = 0;
        StartCoroutine(AsynchronousLoad(slider));
    }

       
    public IEnumerator AsynchronousLoad (Slider slider)
    {
        yield return null;
 
        AsyncOperation ao = SceneManager.LoadSceneAsync(manager.sceneQue);
        ao.allowSceneActivation = this;
 
        while (! ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            slider.value = Mathf.Clamp01(ao.progress / 0.9f);
            
            yield return null;
        }
    }
}