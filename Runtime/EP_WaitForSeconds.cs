using System;
using System.Collections;
using UnityEngine;

public class EP_WaitForSeconds : EventProcedure
{
    public float timeInSeconds;
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        StartCoroutine(WaitForSeconds(timeInSeconds));
        
    }

    private IEnumerator WaitForSeconds(float SecondsDelay)
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(SecondsDelay);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
        controller.nextEvent.Invoke();    
    }

    public override void OnEnd()
    {
    }
}