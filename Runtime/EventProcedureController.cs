﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class EventProcedureController : MonoBehaviour
{
    public ExperimentManager manager;
    private int ParticipantNumber => manager.participantId;
    
    [SerializeField] private bool logInUnity;
    [SerializeField] private bool logInFile;
    
    [SerializeReference]
    public EventProcedure[] events;
    [SerializeReference]
    public EventProcedure debugEvent;

    private int currentEventId;
    [HideInInspector] public UnityEvent nextEvent;

    private string timeString;
    private StreamWriter file;

    public void StartSetFile()
    {
        file?.Close();
        timeString = DateTime.Now.ToString("yyyyMMdd-HHmmss");
        var path = @"" + timeString + "-Participant" + ParticipantNumber + ".txt";
        file = new StreamWriter(path, true) {AutoFlush = true};
    }

    private void Awake()
    {
        //events = GetComponentsInChildren<EventProcedure>(true);
    }

    private void Start()
    {
        StartSetFile();
        CallOnEndEvents();

        currentEventId = 0;
        nextEvent = new UnityEvent();
        nextEvent.AddListener(NextStep);
        events[currentEventId].OnStart(this);
    }

    public void Update()
    {
        if (Input.GetButtonUp("Jump"))
            manager.LoadNextScene();
    }

   

    private void OnDisable()
    {
        file?.Close();
    }

    private void NextStep()
    {
        events[currentEventId].OnEnd();
        currentEventId++;
        if (currentEventId < events.Length)
        {
            events[currentEventId].OnStart(this);
        }
        else
        {
            Reset();
        }
    }

    public void Skip()
    {
        NextStep();
    }

    public void StartDebug()
    {
        CallOnEndEvents();
        if (debugEvent != null)
            debugEvent.OnStart(this);
    }

    private void CallOnEndEvents()
    {
        foreach (var eventProcedure in events)
        {
            eventProcedure.OnEnd();
        }

        if (debugEvent != null)
            debugEvent?.OnEnd();
    }

    public void Reset()
    {
        CallOnEndEvents();
        Start();
    }

    public void Log(string logMessage)
    {
        logMessage = SceneManager.GetActiveScene().name + " " + logMessage;
        if (logInUnity)
            Debug.Log(logMessage);

        if (logInFile)
        {
            file.WriteLine(DateTime.Now.ToString("HHmmss") + " " + logMessage);
        }
    }

    public void StartCertainEvent(int index)
    {
        events[currentEventId].OnEnd();
        currentEventId = index;
        if (currentEventId < events.Length)
            events[currentEventId].OnStart(this);
        else
        {
            Reset();
        }
        
    }
}
