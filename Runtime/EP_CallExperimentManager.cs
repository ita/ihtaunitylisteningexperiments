﻿using System;
using UnityEngine;

[Serializable]
public class EP_CallExperimentManager : EventProcedure
{
    [SerializeField] private ExperimentManager manager;
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        manager.LoadNextScene();
        OnEnd();
    }

    public override void OnEnd()
    {
    }
}