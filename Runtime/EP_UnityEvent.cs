using UnityEngine.Events;

public class EP_UnityEvent:EventProcedure
{
    public UnityEvent onStartEvent;
    public bool state;
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;

        //
        onStartEvent.Invoke();
        controller.nextEvent.Invoke();
    }

    public override void OnEnd()
    {
    }
}