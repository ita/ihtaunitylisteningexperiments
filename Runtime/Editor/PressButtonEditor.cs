﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PressButton))]
public class PressButtonEditor : Editor
{
    PressButton button;

    public void OnEnable()
    {
        button = (PressButton)target;
        
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("ClickButton")) 
            button.OnClick();
    }
}