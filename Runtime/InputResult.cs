﻿using UnityEngine;

public abstract class InputResult : MonoBehaviour
{
    public abstract string Result { get; }
    public abstract bool ResultGiven { get; }
    public abstract void Start();
    public abstract void Reset();
}