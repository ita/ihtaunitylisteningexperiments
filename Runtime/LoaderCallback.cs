﻿using System;
using UnityEngine;

public class LoaderCallback : MonoBehaviour
{
    public ExperimentManager manager;

    private bool isFirstUpdate = true;

    private void Update()
    {
        if (isFirstUpdate)
        {
            isFirstUpdate = false;
            manager.LoadingCallback();
        }
    }
}