﻿using System;
using UnityEngine;

[Serializable]
public class EP_SetSceneSetting : EventProcedure
{
    [SerializeField] private ExperimentManager manager;
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
    }

    public void PressNextButton()
    {
        nextEvent.Invoke();
        OnEnd();
    }
    
    // private void SetSceneSettings()
    // {
    //     //trackBehaviour.GetVisualDefault();
    //     switch (manager.sceneSetting)
    //     {
    //         case ExperimentSetting.AudioVisual:
    //             //trackBehaviour.SetAudible(true);
    //             //trackBehaviour.SetVisual(true);
    //             break;
    //
    //         case ExperimentSetting.Audio:
    //             //trackBehaviour.SetAudible(true);
    //             //trackBehaviour.SetVisual(false);
    //             break;
    //
    //         case ExperimentSetting.Visual:
    //             //trackBehaviour.SetAudible(false);
    //             //trackBehaviour.SetVisual(true);
    //             break;
    //
    //     }
    // }

    public override void OnEnd()
    {
    }
}