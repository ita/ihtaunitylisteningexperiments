﻿using System;
using UnityEngine;


[Serializable]
public class EP_StartCanvas : EventProcedure
{
   
    [SerializeField] private GameObject canvas;
    
    
    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        canvas.SetActive(true);
    }

    public void PressNextButton()
    {
        nextEvent.Invoke();
        OnEnd();
    }

    public override void OnEnd()
    {
        canvas.SetActive(false);
    }
}