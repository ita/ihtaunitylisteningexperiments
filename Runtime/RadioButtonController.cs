﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UI;


public class RadioButtonController : InputResult
{
    public Toggle[] toggles;
    public int currentStatus;

    public override string Result => ResultInt.ToString();
    public override bool ResultGiven => ResultInt != -1;

    private int ResultInt
    {
        get
        {
            for (var i = 0; i < toggles.Length; i++)
            {
                if (toggles[i].isOn)
                    return i;
            }

            return -1;
        }
    }

    // Start is called before the first frame update
    public override void Start()
    {
        Reset();
    }

    public override void Reset()
    {
        currentStatus = -1;
        foreach (var toggle in toggles)
        {
            toggle.isOn = false;
        }
    }
}
