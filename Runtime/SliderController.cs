﻿using UnityEngine;
using UnityEngine.UI;

public class SliderController : InputResult
{
    [SerializeField] private Slider slider;
    public override string Result => (slider.value * 2f - 1f).ToString("F2");
    public override bool ResultGiven => true;

    public override void Start()
    {
        Reset();
    }

    public override void Reset()
    {
        slider.value = 0.5f;
    }
}