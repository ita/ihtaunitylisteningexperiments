﻿using System.Collections;
using UnityEngine;
using VAUnity;

public class EP_StartSound : EventProcedure
{
    [SerializeField] private VAUSoundSource vaSoundSource;

    public override void OnStart(EventProcedureController controller)
    {
        this.controller = controller;
        vaSoundSource.onSoundStops.AddListener(nextEvent.Invoke);
        vaSoundSource.Play();
    }

    public override void OnEnd()
    {
    }
}