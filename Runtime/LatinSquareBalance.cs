﻿using System;
using System.Dynamic;
using UnityEngine;

[Serializable]
public class LatinSquareBalance
{
    [SerializeField] private bool ignoreLatinSquare;
    [SerializeField] private CodeList codeList;
    [SerializeField] private LatinSquare latinSquare;

    public int Length => codeList.Length;

    public string GetBalancedResult(int participantNumber, int currentNumber)
    {
        if (ignoreLatinSquare)
            return codeList.codeList[currentNumber % codeList.Length];
        if (latinSquare == null || latinSquare.order != codeList.Length)
            latinSquare=new LatinSquare(codeList.Length);

        return codeList.codeList[latinSquare.GetIndex(participantNumber,currentNumber)];
    }

   
}