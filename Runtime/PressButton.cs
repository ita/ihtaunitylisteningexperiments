﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressButton : MonoBehaviour
{
    public void OnClick()
    {
        var button = GetComponent<Button>();
        button.onClick.Invoke();
    }
}
